package com.icon.proceso;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.icon.DTO.DatosSolicitudesDTO;
import com.icon.utils.Utilidades;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Main {

	/**
	 * @param args
	 */
       
    private static final Logger log = Logger.getLogger(Main.class);
         
	public static void main(String[] args) {
		try {
                        //PropertyConfigurator.configure("D:\\NotificaSPN\\log4j.properties");
                        PropertyConfigurator.configure("C:\\Users\\NXT00005\\Desktop\\log4j.properties");
                        Main m = new Main();
			System.out.println("Main");
                        log.info("Main");
			List<DatosSolicitudesDTO> solicitudes = m.obtieneSolicitudes();
			ProcesaNotificaciones pn = new ProcesaNotificaciones(solicitudes);
			pn.start();
			boolean flag = true;
			while (flag) {
				if (!pn.isAlive()) {
					flag = false;
				} else {
					Thread.sleep(5000);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
                        log.fatal(e);
		}
	}

	public List<DatosSolicitudesDTO> obtieneSolicitudes() throws Exception {

		List<DatosSolicitudesDTO> listaSolicitudes = new ArrayList<DatosSolicitudesDTO>();
		//String sql = "select id_solicitud, num_telefono, tcnotificaspngacparamid, tcnotificaspngacbitacoraid from TC_NOTIFICA_SPN_GAC_BITACORA where ESTADO='I'";
                String sql = "select id_solicitud, num_telefono, tcnotificaspngacparamid, tcnotificaspngacbitacoraid from TC_NOTIFICA_SPN_GAC_BITACORA where ID_SOLICITUD=2134596";

                log.info("Obtiene listado de notificaciones a enviar");
                log.debug(sql);
                Connection con = Utilidades.getConexion(Utilidades.SQL_CONEXION);
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			try {
				ResultSet rs = ps.executeQuery();
				try {
					while (rs.next()) {
						DatosSolicitudesDTO datosS = new DatosSolicitudesDTO();
						datosS.setIdSolicitud(rs.getString(1));
						datosS.setNumTelefono(rs.getString(2));
						datosS.setTcnotificaspngacparamid(rs.getString(3));
						datosS.setTcnotificaspngacbitacoraid(rs.getString(4));
						listaSolicitudes.add(datosS);
						System.out.println(datosS.getIdSolicitud());
                                                log.debug(datosS.getIdSolicitud());
					}
				} finally {
					rs.close();
				}
			} finally {
				ps.close();
			}

		} finally {
			con.close();
		}

		return listaSolicitudes;

	}

}
