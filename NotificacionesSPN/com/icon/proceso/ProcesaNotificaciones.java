package com.icon.proceso;

import java.util.List;

import com.icon.DTO.DatosCasoDTO;
import com.icon.DTO.DatosNumeroDTO;
import com.icon.DTO.DatosSolicitudesDTO;
import com.icon.consultas.DatosCaso;
import com.icon.consultas.DatosNumero;
import com.icon.utils.Utilidades;
import org.apache.log4j.Logger;

public class ProcesaNotificaciones extends Thread {

    //private static String SQL_NO_DES_ERROR = "select des_error from PTB_SOLIC_PORTABILIDAD_DET_TBO where ID_SOLPREV_SPN = ?";
    private static String SQL_DES_ERROR = "select des_error from PTB_SOLIC_PORTABILIDAD_DET_TBO where ID_PREVAL_OPE = ?";

    private static String SQL_FVC = "select fvc_negociada from PTB_SOLIC_PORTABILIDAD_DET_TBO where ID_SOLPORT_SPN = ?";

    private static String FALTA_DE_PAGO = "FaltaDePago: ";

    private static String MENSAJE_MORA = "La solicitud de Portabilidad ha sido rechazada. El Operador al cual pertenece este numero lo  reportó  Suspendido por Mora. Inicie nuevamente el proceso de portabilidad una vez cancelada la deuda con su Operador actual.";

    private static String MENSAJE_NOMBRE = "La solicitud de Portabilidad  ha sido  rechazada, porque el número de identificaci�n, o nombre no corresponden con el registro  del Operador actual.";

    private static String NOMBRE = "Nombre: ";

    List<DatosSolicitudesDTO> solicitudes;

    public ProcesaNotificaciones(List<DatosSolicitudesDTO> solicitudes) {
        this.solicitudes = solicitudes;
    }

    public void run() {
        procesa();
    }

    private static Logger log = Logger.getLogger(ProcesaNotificaciones.class);

    public void procesa() {
        try {

            Utilidades utils = new Utilidades();
            DatosCasoDTO datosCasos = null;

            DatosCaso datosC = new DatosCaso();
            DatosNumero datosN = new DatosNumero();
            DatosNumeroDTO datosNumero = new DatosNumeroDTO();
            String comentario = "";
            String fechaVC = "";
            String resp = "";
            String numMoviDefault = utils.getNumeroDefaultPospago();
            String mensajeSms = null;
            String mensajeSms2 = null;
            for (DatosSolicitudesDTO listSolicitudes : solicitudes) {
                boolean crearCaso = false;
                boolean enviaSMS = false;
                String idsolicitud = listSolicitudes.getIdSolicitud();
                System.out.println(idsolicitud);

                datosNumero = datosN.datosTelefonoPortar(
                        listSolicitudes.getNumTelefono(), numMoviDefault);

                String datosComentario = "ID_portabilidad: " + datosNumero.idPorta
                        + "IDSolicitud: " + idsolicitud
                        + "; Nombre completo: " + datosNumero.getNombre() + ";"
                        + " Tipo Documento Personal: "
                        + datosNumero.getDesIdenti() + ";"
                        + " Numero Identificacion: "
                        + datosNumero.getNoIdenti() + ";";

                System.out.println("datosComentario :" + datosComentario);
                log.info("datosComentario :" + datosComentario);
                // Se valida si se tiene que parsear el campo
                // des_error
                System.out.println("getTcnotificaspngacparamid() :"
                        + listSolicitudes.getTcnotificaspngacparamid());
                log.info("getTcnotificaspngacparamid() :" + listSolicitudes.getTcnotificaspngacparamid());
                System.out.println("ESTADO PORTABILIDAD ACTUAL: NUMERO: " + datosNumero.getNumTel() + " COD_ESTADO: " + datosNumero.getCod_estado() + " COD_SUBESTADO: " + datosNumero.getCod_subestado());
                if ((datosNumero.getCod_estado().endsWith("SOLPOR")
                        && (datosNumero.getCod_subestado().equals("1") || datosNumero.getCod_subestado().equals("2") || datosNumero.getCod_subestado().equals("0")))
                        || (datosNumero.getCod_estado().endsWith("FVC"))
                        || (datosNumero.getCod_estado().endsWith("SOLPREV"))
                        && (datosNumero.getCod_subestado().equals("1") || datosNumero.getCod_subestado().equals("2") || datosNumero.getCod_subestado().equals("0")
                        || datosNumero.getCod_subestado().equals("3") || datosNumero.getCod_subestado().equals("4"))) {
                    System.out.println("actualizaTabla :");
                    log.info("actualizaTabla :");
                    resp = "PORTABILIDAD ENVIADA";
                } else {
                    datosCasos = datosC.getDatosCasoEstado(listSolicitudes
                            .getTcnotificaspngacparamid());
                    System.out.println("datosCasos.getDesError :"
                            + datosCasos.getDesError());

                    log.info("datosCasos.getDesError :" + datosCasos.getDesError());

                    if (datosCasos.getDesError().equals("N")) {
                        // Se valida si debe obtener FVC
                        if (datosCasos.getSMS() != null
                                && !datosCasos.getSMS().trim().equals("")) {
                            mensajeSms = datosCasos.getSMS();
                            enviaSMS = true;
                        }
                        if ((listSolicitudes.getTcnotificaspngacparamid().equals("7") || listSolicitudes.getTcnotificaspngacparamid().equals("3")
                                || listSolicitudes.getTcnotificaspngacparamid().equals("23")) && datosNumero.getCod_canal().equals("WEB")) {
                            utils.enviaMail("https://www2.movistar.cr/prepaid-web-portability/api/emails/nip-code-not-valid/?email=" + datosNumero.getMail() + "&phone=" + datosNumero.getNumTel() + "&first_name=" + datosNumero.getNom_usu() + "&surname=" + datosNumero.getApe1_usu() + "&second_surname=" + datosNumero.getApe2_usu());
                        }

                        if ((listSolicitudes.getTcnotificaspngacparamid().equals("61") || listSolicitudes.getTcnotificaspngacparamid().equals("62")) && datosNumero.getCod_canal().equals("WEB")) {
                            utils.enviaMail("https://www2.movistar.cr/prepaid-web-portability/api/emails/line-data-not-found/?email=" + datosNumero.getMail() + "&phone=" + datosNumero.getNumTel() + "&first_name=" + datosNumero.getNom_usu() + "&surname=" + datosNumero.getApe1_usu() + "&second_surname=" + datosNumero.getApe2_usu());
                        }

                        if (datosCasos.getFVC().equals("S")) {
                            fechaVC = utils.ejecutaConsulta(SQL_FVC,
                                    String.valueOf(idsolicitud));
                            System.out.println(SQL_FVC);
                            log.info(SQL_FVC);
                            if (fechaVC != null) {
                                comentario = "La FVC es: " + fechaVC;
                                datosCasos.setComentario(comentario);
                                if (mensajeSms != null) {
                                    mensajeSms = mensajeSms.replaceAll("#FVC#",
                                            fechaVC);
                                }
                                crearCaso = true;
                                enviaSMS = true;
                            }
                        } else {
                            /*comentario = utils.ejecutaConsulta(SQL_NO_DES_ERROR,
								String.valueOf(idsolicitud));
						System.out.println(SQL_NO_DES_ERROR);*/

                            datosCasos.setComentario(mensajeSms);
                            //mensajeSms = comentario;
                            crearCaso = true;
                            enviaSMS = true;
                        }
                    } else {
                        if (datosNumero.getCod_estado().equals("SOLPREV") && datosNumero.getCod_subestado().equals("5")
                                && datosNumero.getCod_canal().equals("WEB")) {
                            utils.enviaMail("https://www2.movistar.cr/prepaid-web-portability/api/emails/hrs-to-process/?email=" + datosNumero.getMail() + "&phone=" + datosNumero.getNumTel() + "&first_name=" + datosNumero.getNom_usu() + "&surname=" + datosNumero.getApe1_usu() + "&second_surname=" + datosNumero.getApe2_usu());
                            utils.enviarSMS(datosNumero.getNumTel(), "Tu solicitud de portabilidad ha sido preaprobada, para finalizar tu tramite debes visitar una de nuestras tiendas en 24 horas donde te daremos tu SIM Movistar.");
                        } else {
                            if (listSolicitudes.getIdSolicitud().equals(datosNumero.getPreval_ope())) {
                                String descError = utils.ejecutaConsulta(SQL_DES_ERROR,
                                        idsolicitud);
                                System.out.println(SQL_DES_ERROR);

                                // Se verifica que la respuesta tenga el valor
                                // EstadoRespuestaDonante: 1 Si se encuentra se
                                // parsea para encontrar el mativo, de lo
                                // contrario no se procesa
                                if (descError != null
                                        && descError.indexOf("EstadoRespuestaDonante: 1") > -1) {

                                    System.out.println("EstadoRespuestaDonante: 1");
                                    log.info("EstadoRespuestaDonante: 1");
                                    String errorPago = utils.parsea(descError,
                                            FALTA_DE_PAGO);
                                    String errorDatos = utils.parsea(descError, NOMBRE);

                                    if (errorPago.equals("1") && errorDatos.equals("1")) {
                                        log.debug("Entra a error pago y nombre");
                                        log.info("Genera caso: " + datosCasos.getSMS() + MENSAJE_MORA + " " + MENSAJE_NOMBRE);
                                        datosCasos.setComentario(datosCasos.getSMS()
                                                + MENSAJE_MORA + " " + MENSAJE_NOMBRE);
                                        mensajeSms = MENSAJE_MORA;
                                        mensajeSms2 = MENSAJE_NOMBRE;
                                        crearCaso = true;
                                        enviaSMS = true;
                                    } else if (errorPago.equals("1")) {
                                        log.debug("Entra a error pago");
                                        log.info("Genera caso: " + datosCasos.getSMS() + MENSAJE_MORA);
                                        datosCasos.setComentario(datosCasos.getSMS()
                                                + MENSAJE_MORA);
                                        mensajeSms = MENSAJE_MORA;
                                        crearCaso = true;
                                        enviaSMS = true;
                                    } else if (errorDatos.equals("1")) {
                                        log.debug("Entra a error nombre");
                                        log.info("Genera caso: " + datosCasos.getSMS() + MENSAJE_NOMBRE);
                                        datosCasos.setComentario(datosCasos.getSMS()
                                                + MENSAJE_NOMBRE);
                                        mensajeSms = MENSAJE_NOMBRE;
                                        crearCaso = true;
                                        enviaSMS = true;
                                    }

                                } else {
                                    System.out.println("EstadoRespuestaDonante:2");
                                    log.info("EstadoRespuestaDonante:2");
                                    resp = " --No se procesa por no contar con respuesta: idSolicitud: "
                                            + idsolicitud;
                                }
                            } else {
                                resp = "Prevalidación automática";
                                crearCaso = false;
                                enviaSMS = false;
                            }
                        }
                    }
                }
                System.out.println("crearCaso :" + crearCaso);
                log.info("crearCaso :" + crearCaso);
                System.out.println("enviaSMS :" + enviaSMS);
                log.info("enviaSMS :" + enviaSMS);
                if (crearCaso) {
                    System.out.println("crearCasoGAC :" + crearCaso);
                    log.info("crearCasoGAC :" + crearCaso);
                    log.info("Comentario caso: " + datosCasos.getComentario() + " " + datosNumero.getNumTel());
                    String respuestaCaso = utils.crearCasoGAC(datosCasos,
                            datosNumero);
                    resp = respuestaCaso + " IDSolicitud: " + idsolicitud;
                }
                if (enviaSMS) {
                    if (mensajeSms != null) {
                        System.out.println("enviarSMS :" + enviaSMS);
                        log.info("enviarSMS :" + enviaSMS);
                        utils.enviarSMS(datosNumero.getNumTel(), mensajeSms);
                        if (datosNumero.isPoseeNumeroMo()) {
                            utils.enviarSMS(datosNumero.getNumCelMo(),
                                    mensajeSms);
                        }

                    }
                    if (mensajeSms2 != null) {
                        System.out.println("enviarSMS :" + mensajeSms2);
                        log.info("enviarSMS :" + mensajeSms2);
                        utils.enviarSMS(datosNumero.getNumTel(), mensajeSms2);
                        if (datosNumero.isPoseeNumeroMo()) {
                            utils.enviarSMS(datosNumero.getNumCelMo(),
                                    mensajeSms2);
                        }
                    }
                }
                System.out.println("actualizaTabla :");
                log.info("actualizaTabla :");
                utils.actualizaTabla(resp,
                        listSolicitudes.getTcnotificaspngacbitacoraid());
                System.out.println(resp);
                log.info(resp);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            log.fatal(e);
        }
    }
}
