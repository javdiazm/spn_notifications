/*
 * Creado el 3/03/2012
 * 
 * TODO Para cambiar la plantilla de este archivo generado, vaya a Ventana -
 * Preferencias - Java - Estilo de codigo - Plantillas de codigo
 */
package com.icon.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.icon.gac.ServicioCasosProxy;

public class ProxyWS extends Utilidades {

	public static ServicioCasosProxy setServicioCasosProxy() {
		System.out.println("setServicioCasosProxy :");
		ServicioCasosProxy proxy = null;
		System.out.println("Proxy ");
		try {

			String query = "SELECT WEBSERVICE_PATH "
					+ " FROM TC_WEBSERVICE_PATH "
					+ " WHERE UPPER(NOMBRE) = UPPER('WS_SERVICIO_CASOS')";

			Connection cn = getConexion(SQL_CONEXION_SCL);

			try {
				Statement st = cn.createStatement();
				try {
					ResultSet rs = st.executeQuery(query);
					try {
						if (rs.next()) {
							System.out.println("Proxy :"+rs.getString(1));
							proxy = new ServicioCasosProxy(rs.getString(1));
							
							System.out.println("fin Proxy ");

						}
					} finally {
						rs.close();
					}
				} finally {
					st.close();
				}
			} finally {
				cn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return proxy;
	}

}
