/*
 * Created on Jul 22, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.icon.utils;

import java.io.FileInputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.net.*;
import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.OutputStream;

import com.icon.DTO.DatosCasoDTO;
import com.icon.DTO.DatosNumeroDTO;
import com.icon.gac.ServicioCasos;
import java.io.IOException;
import java.util.logging.Level;
import javax.net.ssl.SSLHandshakeException;
import org.apache.log4j.Logger;

/**
 * @author fvcg
 *
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class Utilidades {

    /**
     * Variable que almacena el nombre de la referencia a los recursos de la BDD
     * SCL
     */
    private static Logger log = Logger.getLogger(Utilidades.class);

    public static final String SQL_CONEXION_SCL = "scl";

    public static final String SQL_CONEXION = "gac_cr";

    private static final String QUERY_POSTPAGO = "select dato from tc_parametros_porta"
            + " where parametro = 'NOPOSTPAGO_PORTABILIDAD'";

    private static final String QUERY_ACTUALIZA_TABLA = "UPDATE TC_NOTIFICA_SPN_GAC_BITACORA SET ESTADO = 'F', respuesta = ? WHERE tcnotificaspngacbitacoraid = ?";

    /**
     * Devuelve la conexion de una referencia a recursos definida en el
     * descriptor de despliegue
     *
     * @param nombre Nombre de la referencia a los recursos
     */
    public static Connection getConexion(String base)
            throws ClassNotFoundException, SQLException {

        Class.forName("oracle.jdbc.driver.OracleDriver");

        Connection cnn = null;

        cnn = DriverManager.getConnection(getValor(base));

        return cnn;
    }

    protected static Properties propiedades;

    public static String getValor(String llave) {
        try {

            if (propiedades == null) {

                propiedades = new Properties();
                //propiedades.load(new FileInputStream("D:\\NotificaSPN\\conf.properties"));
                propiedades.load(new FileInputStream("C:\\Users\\NXT00005\\Desktop\\conf.properties"));
            }
            return propiedades.getProperty(llave).trim();

        } catch (Exception ex) {
            return null;
        }

    }

    public String getNumeroDefaultPospago() {
        String numpost = "";
        try {
            Connection con = getConexion(SQL_CONEXION_SCL);
            try {
                PreparedStatement ps = con.prepareStatement(QUERY_POSTPAGO);
                try {
                    ResultSet rs = ps.executeQuery();
                    try {
                        if (rs.next()) {
                            numpost = rs.getString("dato");
                        } else {
                            numpost = "";
                        }
                    } finally {
                        rs.close();
                    }
                } finally {
                    ps.close();
                }
            } finally {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.fatal(e);
        }
        return numpost;
    }

    public String ejecutaConsulta(String sql, String param) {
        String resp = "";
        try {
            System.out.println("ejecutaConsulta :" + sql);
            log.debug("ejecutaConsulta :" + sql);
            System.out.println("param :" + param);
            log.debug("param :" + param);
            Connection con = getConexion(SQL_CONEXION_SCL);
            try {
                PreparedStatement ps = con.prepareStatement(sql);
                try {
                    ps.setString(1, param);
                    ResultSet rs = ps.executeQuery();
                    try {
                        if (rs.next()) {
                            resp = rs.getString(1);
                        }
                    } finally {
                        rs.close();
                    }
                } finally {
                    ps.close();
                }
            } finally {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.fatal(e);
        }
        return resp;
    }

    public String crearCasoGAC(DatosCasoDTO datosCasos,
            DatosNumeroDTO datosNumero) {
        System.out.println("crearCasoGAC :");
        log.info("**************************CREARCASOGAC*********************************");
        ServicioCasos srv = ProxyWS.setServicioCasosProxy();
        String codError = null;
        String casoid = null;
        String fechError = null;
        String respuesta = "";
        if (datosCasos.getComentario().isEmpty()) {
            datosCasos.setComentario("Caso creado automaticamente");
        }
        try {
            System.out.println(" Inicio crearCasoEscalado :");
            log.info(" Inicio crearCasoEscalado :");
            System.out.println(datosCasos.getTipologiaid() + "  "
                    + datosCasos.getUsuario() + "  " + datosCasos.getComentario() + "  "
                    + datosNumero.getNumCelMo() + "  " + datosCasos.getUnidadid() + "  "
                    + datosNumero.getNumTel() + "  " + datosCasos.getBuzonid());
            log.info("Datos del caso: " + datosCasos.getTipologiaid() + "  "
                    + datosCasos.getUsuario() + "  " + datosCasos.getComentario() + "  "
                    + datosNumero.getNumCelMo() + "  " + datosCasos.getUnidadid() + "  "
                    + datosNumero.getNumTel() + "  " + datosCasos.getBuzonid());
            String res = srv.crearCasoEscalado(datosCasos.getTipologiaid(),
                    datosCasos.getUsuario(), datosCasos.getComentario(),
                    datosNumero.getNumCelMo(), datosCasos.getUnidadid(),
                    datosNumero.getNumTel(), datosCasos.getBuzonid());

            System.out.println("Respuesta de creacion de caso: " + res);
            log.info("Respuesta de creacion de caso: " + res);

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource archivo = new InputSource();
            archivo.setCharacterStream(new StringReader(res));

            Document documento = db.parse(archivo);
            documento.getDocumentElement().normalize();

            NodeList nodeLista = documento.getElementsByTagName("respuesta");
            log.debug("Analisis respuesta: " + documento.toString());
            for (int s = 0; s < nodeLista.getLength(); s++) {
                Node primerNodo = nodeLista.item(s);
                if (primerNodo.getNodeType() == Node.ELEMENT_NODE) {

                    codError = valorXML("codigo-error", primerNodo);
                    fechError = valorXML("fecha-hora", primerNodo);

                    Element primerElemento = (Element) primerNodo;
                    NodeList casoLista = primerElemento
                            .getElementsByTagName("caso");
                    for (int j = 0; j < casoLista.getLength(); j++) {
                        Node NodoCaso = nodeLista.item(s);
                        if (NodoCaso.getNodeType() == Node.ELEMENT_NODE) {
                            casoid = valorXML("casoid", NodoCaso);
                        }
                    }
                }
            }
            respuesta = " --CodError GAC: " + codError + " Casoid: " + casoid
                    + " Fecha: " + fechError;
            log.info(respuesta);
        } catch (Exception e) {
            System.out.println("crearCasoGAC :" + e.getMessage());
            e.printStackTrace();
            log.fatal("crearCasoGAC :" + e);
        }

        return respuesta;
    }

    public String valorXML(String tag, Node primerNodo) {
        String respuesta = "";
        Element primerElemento = (Element) primerNodo;

        NodeList nodo = primerElemento.getElementsByTagName(tag);
        Element elemento = (Element) nodo.item(0);
        NodeList nodoLista = elemento.getChildNodes();
        respuesta = ((Node) nodoLista.item(0)).getNodeValue().toString();

        System.out.println("respuesta : " + respuesta);
        log.debug("Obtiene XML - TAG:" + tag + " NODO: " + primerNodo.toString() + " RESPUESTA: " + respuesta);
        return respuesta;
    }

    public String parsea(String texto, String param) {
        String respuesta = "";
        int inicio = 0;
        int fin = 0;

        inicio = texto.indexOf(param);
        fin = inicio + param.length();
        respuesta = texto.substring(fin, fin + 1);

        return respuesta;

    }

    public void enviarSMS(String telefono, String texto) {
        String result = "";
        System.out.println("Enviando SMS telefono: " + telefono + " texto: "
                + texto);
        log.info("Enviando SMS telefono: " + telefono + " texto: "
                + texto);
        // String enviar = "SELECT SYSDATE FROM DUAL";

        String enviar = "SELECT UTL_HTTP.request('http://10.34.1.90:13651/cgi-bin/sendsms?"
                + "username=portabilidad'||chr(38)||'password=portabilidad'||chr(38)||'from=50663001693'||chr(38)||'to=506"
                + telefono
                + "'||chr(38)||'text='||UTL_URL.ESCAPE('"
                + texto
                + "')) RESULT FROM DUAL";

        System.out.println(enviar);
        log.debug(enviar);

        try {
            Connection con = getConexion(SQL_CONEXION_SCL);
            try {
                PreparedStatement ps = con.prepareStatement(enviar);
                try {
                    ResultSet rs = ps.executeQuery();
                    try {
                        if (rs.next()) {
                            result = rs.getString(1);
                        }
                    } finally {
                        rs.close();
                    }
                    System.out.println("Se envio el SMS: " + result);
                    log.info("Se envio el SMS: " + result);
                } finally {
                    ps.close();
                }
            } finally {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public int actualizaTabla(String respuesta, String idTabla) {
        int i = 0;
        try {
            Connection con = getConexion(SQL_CONEXION);
            try {
                PreparedStatement ps = con
                        .prepareStatement(QUERY_ACTUALIZA_TABLA);
                System.out.println("QUERY_ACTUALIZA_TABLA: "
                        + QUERY_ACTUALIZA_TABLA + " RESPUESTA: " + respuesta + " ID_TABLA: " + idTabla);
                log.debug("QUERY_ACTUALIZA_TABLA: "
                        + QUERY_ACTUALIZA_TABLA + " RESPUESTA: " + respuesta + " ID_TABLA: " + idTabla);
                try {
                    ps.setString(1, respuesta);
                    ps.setString(2, idTabla);
                    i = ps.executeUpdate();
                    System.out.println(i);
                } finally {
                    ps.close();
                }
            } finally {
                con.close();
            }
        } catch (Exception e) {
            i = 0;
            e.printStackTrace();
            log.fatal(e);
        }
        return i;
    }

    public boolean enviaMail(String dir) {
        BufferedReader in = null;
        log.debug(dir);
        System.out.println(dir);
        try {
            truster();
            URL yahoo = new URL(dir);
            URLConnection yc = yahoo.openConnection();
            in = new BufferedReader(
                    new InputStreamReader(
                            yc.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.contains("All ok")) {
                    in.close();
                    return true;
                } else {
                    in.close();
                    return false;
                }
            }   return false;
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
                return false;
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }

    }

    public void truster() {

        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {

                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    //No need to implement.
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    //No need to implement.
                }
            }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
