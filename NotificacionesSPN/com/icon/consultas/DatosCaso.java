package com.icon.consultas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.icon.DTO.DatosCasoDTO;
import com.icon.utils.Utilidades;

public class DatosCaso extends Utilidades {

	private static final String QUERY_DATOS_CASO_ESTADO = "select TIPOLOGIAID,BUZONID,UNIDADID, USUARIO,SMS, DESC_ERROR, FVC "
			+ " from TC_NOTIFICA_SPN_GAC_PARAM "
			+ " where tcnotificaspngacparamid = ?  ";

	public DatosCasoDTO getDatosCasoEstado(String idSolicitud) {
		DatosCasoDTO datosCaso = new DatosCasoDTO();
		System.out.println("getDatosCasoEstado :" + idSolicitud);
		try {
			Connection con = getConexion(SQL_CONEXION);
			try {
				PreparedStatement ps = con
						.prepareStatement(QUERY_DATOS_CASO_ESTADO);
				try {
					ps.setString(1, idSolicitud);
					ResultSet rs = ps.executeQuery();

					try {
						if (rs.next()) {
							datosCaso.setTipologiaid(rs.getString(1));
							datosCaso.setBuzonid(rs.getString(2));
							datosCaso.setUnidadid(rs.getString(3));
							datosCaso.setUsuario(rs.getString(4));
							datosCaso.setSMS(rs.getString(5));
							datosCaso.setDesError(rs.getString(6));
							datosCaso.setFVC(rs.getString(7));
						} else {
							datosCaso = null;
						}
					} finally {
						rs.close();
					}
				} finally {
					ps.close();
				}
			} finally {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return datosCaso;
	}
}
