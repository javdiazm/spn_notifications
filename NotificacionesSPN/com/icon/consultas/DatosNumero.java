package com.icon.consultas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.icon.DTO.DatosNumeroDTO;
import com.icon.utils.Utilidades;

public class DatosNumero {

	private static final String QUERY_NUM_TELEFONO = "select case when a.ape2_cliente is null then ' ' else a.ape2_cliente end as ape2_cliente,"
                        + " case when a.ape1_cliente is null then ' ' else a.ape1_cliente end as ape1_cliente, case when a.nom1_cliente is null then ' ' else a.nom1_cliente end as nom1_cliente,"
                        + " a.mail_cliente, b.usuario, b.cod_estado, b.cod_subestado, b.id_portabilidad idPorta, b.num_telefono numTelefono, b.num_celular_movistar numMovistar, b.id_preval_ope,"
			+ " a.cod_tipident_cli codigoIdent, d.des_tipident descIdent, a.num_ident_cli numIdent, "
			+ " a.nom1_cliente ||"
			+ " case when a.nom2_cliente is null then ' ' else a.nom2_cliente"
			+ " end||"
			+ " case when a.ape1_cliente is null then ' ' else a.ape1_cliente"
			+ " end|| ' '||"
			+ " case when a.ape2_cliente is null then ' ' else a.ape2_cliente"
			+ " end as nombre"
			+ " from ptb_solic_portabilidad_tbo a, ptb_solic_portabilidad_det_tbo b, ptb_estados_tbo c, ge_tipident d"
			+ " where a.id_portabilidad=b.id_portabilidad"
			+ " and b.cod_estado=c.cod_estado_pvl "
			+ " and a.cod_tipident_cli= d.cod_tipident"
			+ " and b.cod_subestado=c.cod_subestado_pvl"
			+ " and b.num_telefono = ?"
                        + " ORDER BY B.FEC_SUBESTADO DESC";
        

	public DatosNumeroDTO datosTelefonoPortar(String telefono,
			String telefonoMovistar) {

		DatosNumeroDTO datosNumeroDTO = new DatosNumeroDTO();
		String expresion = "^[0-9]*{8}$";

		if (telefono == null || telefono.equals("")) {
			return null;
		}

		if (!telefono.matches(expresion)) {
			return null;
		}

		if (telefono.toString().length() < 8
				|| telefono.toString().length() > 8) {

			return null;
		}

		try {

			Connection con = Utilidades
					.getConexion(Utilidades.SQL_CONEXION_SCL);
			try {
				PreparedStatement ps = con.prepareStatement(QUERY_NUM_TELEFONO);
				System.out.println(QUERY_NUM_TELEFONO);
				try {
					ps.setString(1, telefono);
					ResultSet rs = ps.executeQuery();
					try {
						if (rs.next()) {
							datosNumeroDTO.setIdPorta(rs.getString("idPorta"));
							datosNumeroDTO.setNumTel(rs
									.getString("numTelefono"));
							if (rs.getString("numMovistar") == null
									|| rs.getString("numMovistar").equals("")) {
								datosNumeroDTO.setNumCelMo(telefonoMovistar);
								datosNumeroDTO.setPoseeNumeroMo(false);
							} else {
								datosNumeroDTO.setNumCelMo(rs
										.getString("numMovistar"));
								datosNumeroDTO.setPoseeNumeroMo(true);
							}
							datosNumeroDTO.setCodIdenti(rs
									.getString("codigoIdent"));
							datosNumeroDTO.setDesIdenti(rs
									.getString("descIdent"));
							datosNumeroDTO
									.setNoIdenti(rs.getString("numIdent"));
							datosNumeroDTO.setNombre(rs.getString("nombre"));
                                                        datosNumeroDTO.setCod_estado(rs.getString("cod_estado"));
                                                        datosNumeroDTO.setCod_subestado(rs.getString("cod_subestado"));
                                                        datosNumeroDTO.setCod_canal(rs.getString("usuario"));
                                                        datosNumeroDTO.setMail(rs.getString("mail_cliente"));
                                                        datosNumeroDTO.setNom_usu(rs.getString("nom1_cliente"));
                                                        datosNumeroDTO.setApe1_usu(rs.getString("ape1_cliente"));
                                                        datosNumeroDTO.setApe2_usu(rs.getString("ape2_cliente"));
                                                        datosNumeroDTO.setPreval_ope(rs.getString("id_preval_ope"));
						} else {
							datosNumeroDTO = null;
						}
					} finally {
						rs.close();
					}
				} finally {
					ps.close();
				}
			} finally {
				con.close();
			}
		} catch (Exception e) {
			System.out.println("datosComentario :" + e.getMessage());
			e.printStackTrace();
		}
		return datosNumeroDTO;
	}

}
