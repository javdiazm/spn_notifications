package com.icon.DTO;

public class DatosSolicitudesDTO {

	public String idSolicitud = "";
	public String numTelefono = "";
	public String tcnotificaspngacparamid = "";
	public String tcnotificaspngacbitacoraid = "";

	public String getTcnotificaspngacparamid() {
		return tcnotificaspngacparamid;
	}

	public void setTcnotificaspngacparamid(String tcnotificaspngacparamid) {
		this.tcnotificaspngacparamid = tcnotificaspngacparamid;
	}

	public String getTcnotificaspngacbitacoraid() {
		return tcnotificaspngacbitacoraid;
	}

	public void setTcnotificaspngacbitacoraid(String tcnotificaspngacbitacoraid) {
		this.tcnotificaspngacbitacoraid = tcnotificaspngacbitacoraid;
	}

	public String getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(String idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getNumTelefono() {
		return numTelefono;
	}

	public void setNumTelefono(String numTelefono) {
		this.numTelefono = numTelefono;
	}

}
