package com.icon.DTO;

public class DatosCasoDTO {

	public String tipologiaid = "";
	public String buzonid = "";
	public String unidadid = "";
	public String usuario = "";
	public String desError = "";
	public String FVC = "";
	public String SMS = "";
	public String comentario = "";
	public String descripcion = "";

	public String getDesError() {
		return desError;
	}

	public void setDesError(String desError) {
		this.desError = desError;
	}

	public String getFVC() {
		return FVC;
	}

	public void setFVC(String fVC) {
		FVC = fVC;
	}

	public String getTipologiaid() {
		return tipologiaid;
	}

	public String getSMS() {
		return SMS;
	}

	public void setSMS(String sMS) {
		SMS = sMS;
	}

	public void setTipologiaid(String tipologiaid) {
		this.tipologiaid = tipologiaid;
	}

	public String getBuzonid() {
		return buzonid;
	}

	public void setBuzonid(String buzonid) {
		this.buzonid = buzonid;
	}

	public String getUnidadid() {
		return unidadid;
	}

	public void setUnidadid(String unidadid) {
		this.unidadid = unidadid;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
