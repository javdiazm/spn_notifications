package com.icon.DTO;

public class DatosNumeroDTO {

    public String idPorta = "";
    public String numTel = "";
    public String numCelMo = "";
    public String codIdenti = "";
    public String desIdenti = "";
    public String noIdenti = "";
    public String nombre = "";
    public String idSolicitud = "";
    public boolean poseeNumeroMo = false;
    public String cod_estado = "";
    public String cod_subestado = "";
    public String cod_canal = "";
    public String mail = "";
    public String nom_usu="";
    public String ape1_usu="";
    public String ape2_usu="";
    public String preval_ope="";

    public String getPreval_ope() {
        return preval_ope;
    }

    public void setPreval_ope(String preval_ope) {
        this.preval_ope = preval_ope;
    }
    
    

    public String getNom_usu() {
        return nom_usu;
    }

    public void setNom_usu(String nom_usu) {
        this.nom_usu = nom_usu;
    }

    public String getApe1_usu() {
        return ape1_usu;
    }

    public void setApe1_usu(String ape1_usu) {
        this.ape1_usu = ape1_usu;
    }

    public String getApe2_usu() {
        return ape2_usu;
    }

    public void setApe2_usu(String ape2_usu) {
        this.ape2_usu = ape2_usu;
    }
    
    

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    

    public String getCod_canal() {
        return cod_canal;
    }

    public void setCod_canal(String cod_canal) {
        this.cod_canal = cod_canal;
    }

    public String getCod_estado() {
        return cod_estado;
    }

    public void setCod_estado(String cod_estado) {
        this.cod_estado = cod_estado;
    }

    public String getCod_subestado() {
        return cod_subestado;
    }

    public void setCod_subestado(String cod_subestado) {
        this.cod_subestado = cod_subestado;
    }

    public String getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public boolean isPoseeNumeroMo() {
        return poseeNumeroMo;
    }

    public void setPoseeNumeroMo(boolean poseeNumeroMo) {
        this.poseeNumeroMo = poseeNumeroMo;
    }

    public String getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(String idPorta) {
        this.idPorta = idPorta;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public String getNumCelMo() {
        return numCelMo;
    }

    public void setNumCelMo(String numCelMo) {
        this.numCelMo = numCelMo;
    }

    public String getCodIdenti() {
        return codIdenti;
    }

    public void setCodIdenti(String codIdenti) {
        this.codIdenti = codIdenti;
    }

    public String getDesIdenti() {
        return desIdenti;
    }

    public void setDesIdenti(String desIdenti) {
        this.desIdenti = desIdenti;
    }

    public String getNoIdenti() {
        return noIdenti;
    }

    public void setNoIdenti(String noIdenti) {
        this.noIdenti = noIdenti;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
